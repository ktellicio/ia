"""
  AO PREENCHER ESSE CABECALHO COM O MEU NOME E O MEU NUMERO USP,
  DECLARO QUE SOU A UNICA PESSOA AUTORA E RESPONSAVEL POR ESSE PROGRAMA.
  TODAS AS PARTES ORIGINAIS DESSE EXERCICIO PROGRAMA (EP) FORAM
  DESENVOLVIDAS E IMPLEMENTADAS POR MIM SEGUINDO AS INSTRUCOES
  DESSE EP E, PORTANTO, NAO CONSTITUEM ATO DE DESONESTIDADE ACADEMICA,
  FALTA DE ETICA OU PLAGIO.
  DECLARO TAMBEM QUE SOU A PESSOA RESPONSAVEL POR TODAS AS COPIAS
  DESSE PROGRAMA E QUE NAO DISTRIBUI OU FACILITEI A
  SUA DISTRIBUICAO. ESTOU CIENTE QUE OS CASOS DE PLAGIO E
  DESONESTIDADE ACADEMICA SERAO TRATADOS SEGUNDO OS CRITERIOS
  DIVULGADOS NA PAGINA DA DISCIPLINA.
  ENTENDO QUE EPS SEM ASSINATURA NAO SERAO CORRIGIDOS E,
  AINDA ASSIM, PODERAO SER PUNIDOS POR DESONESTIDADE ACADEMICA.

  Nome : Karlson Tellicio Bezerra de Lima
  NUSP : 11585887

  Referencias: Com excecao das rotinas fornecidas no enunciado
  e em sala de aula, caso voce tenha utilizado alguma referencia,
  liste-as abaixo para que o seu programa nao seja considerado
  plagio ou irregular.

  Referências:
  - Tomei como base o enunciado original:
  https://web.stanford.edu/class/cs221/assignments/blackjack/submission.py
"""

import math
import random
from collections import defaultdict
import util


# **********************************************************
# **            PART 01 Modeling BlackJack                **
# **********************************************************


class BlackjackMDP(util.MDP):
    """
    The BlackjackMDP class is a subclass of MDP that models the BlackJack game as a MDP
    """
    def __init__(self, valores_cartas, multiplicidade, limiar, custo_espiada):
        """
        valores_cartas: list of integers (face values for each card included in the deck)
        multiplicidade: single integer representing the number of cards with each face value
        limiar: maximum number of points (i.e. sum of card values in hand) before going bust
        custo_espiada: how much it costs to peek at the next card
        """
        self.valores_cartas = valores_cartas
        self.multiplicidade = multiplicidade
        self.limiar = limiar
        self.custo_espiada = custo_espiada

    def startState(self):
        """
         Return the start state.
         Each state is a tuple with 3 elements:
           -- The first element of the tuple is the sum of the cards in the player's hand.
           -- If the player's last action was to peek, the second element is the index
              (not the face value) of the next card that will be drawn; otherwise, the
              second element is None.
           -- The third element is a tuple giving counts for each of the cards remaining
              in the deck, or None if the deck is empty or the game is over (e.g. when
              the user quits or goes bust).
        """
        
        # sum, peeked_card, (mult,)*(,#card)
        return (0, None, (self.multiplicidade,) * len(self.valores_cartas))

    def actions(self, state):
        """
        Return set of actions possible from |state|.
        You do not must to modify this function.
        """
        return ['Pegar', 'Espiar', 'Sair']

    # Successor states, their probabilites and eventual rewards
    def succAndProbReward(self, state, action):
        """
        Given a |state| and |action|, return a list of (newState, prob, reward) tuples
        corresponding to the states reachable from |state| when taking |action|.
        A few reminders:
         * Indicate a terminal state (after quitting, busting, or running out of cards)
           by setting the deck to None.
         * If |state| is an end state, you should return an empty list [].
         * When the probability is 0 for a transition to a particular new state,
           don't include that state in the list returned by succAndProbReward.
           
           
           state  : (tuple)  current state of MDP
           action : (string) action to be aplied to MDP
           return : (tuple) ((sum, peeked_card, (mult,)*(,#card)), prob, reward)
        """
        # BEGIN_YOUR_CODE
        
        # Get current state values
        # of [sum of cards face values in hands, next card index (if peeked), deck counter array]
        total, peek, deck_counter = state
        # List of ((newState), prob, reward) tuples
        reachable = []
        
        # Check number of card in current deck (N)
        if (deck_counter == None):
            return reachable
        else:
            N = sum(deck_counter)
        
        # In case of 'Espiar', 
        # return list ((total, card_i, deck_counter), #card_i/#cards, -custo_espiada)
        # for every card i in the deck
        if action == 'Espiar':
            # Two peeks in a row is not allowed
            if(peek != None):
                return []
            reward = -self.custo_espiada
            for idx in range(len(deck_counter)):
                # Skip cards that do not appear in the deck   
                if deck_counter[idx] != 0:
                    prob = deck_counter[idx]/N
                    reachable.append(((total, idx, deck_counter), prob, reward))
            return reachable
        
        # In case of 'Pegar', 
        # return list ((new_total, card_i, new_deck_counter), #card_i/#cards, -custo_espiada)
        # for every card i in the deck
        elif action == 'Pegar':
            reward = 0
            
            # When there is a peeked card index
            if(peek != None):
                # 'Pegar' is deterministic in this case
                prob = 1
                # Sum of all cards face values in hands
                new_total = self.valores_cartas[peek] + total 
                
                # Append end state if sum of cards face values in hands is greater than threshold 
                # Busted!
                if (new_total > self.limiar):
                    new_deck_counter = None
                    reachable.append(((new_total, None, new_deck_counter), prob, reward))
                # Not busted but it is the last card, append end state 
                # Win!
                elif N == 1:
                    new_deck_counter = None
                    reward = new_total
                    reachable.append(((new_total, None, new_deck_counter), prob, reward))
                # Not busted yet, append state resulting from taking peeked card
                else:
                    new_deck_counter = list(deck_counter)
                    new_deck_counter[peek] = new_deck_counter[peek] - 1
                    reachable.append(((new_total, None, tuple(new_deck_counter)), prob, reward))
                

            # When there is not a peeked card index
            else:            
                # Add to reachable all possible states by taking every card in the deck
                for idx in range(len(deck_counter)):
                    # Skip cards that do not appear in the deck        
                    if(deck_counter[idx] != 0):
                        # Number of valores_cartas(idx) cards in the deck over current number of cards
                        prob = deck_counter[idx]/N
                        # Sum of all cards face values in hands
                        new_total = self.valores_cartas[idx] + total
                        
                        # Append end state if sum of cards face values in hands is greater than threshold 
                        # Busted!
                        if (new_total > self.limiar):
                            new_deck_counter = None
                            reachable.append(((new_total, None, new_deck_counter), prob, reward))
                        # Not busted but it is the last card, append end state 
                        # Win!
                        elif N == 1:
                            new_deck_counter = None
                            reward = new_total
                            reachable.append(((new_total, None, new_deck_counter), prob, reward))
                        # Not busted yet, append state resulting from 'Pegar'
                        else:
                            new_deck_counter = list(deck_counter)
                            new_deck_counter[idx] = new_deck_counter[idx] - 1
                            reachable.append(((new_total, None, tuple(new_deck_counter)), prob, reward))
                    
            return reachable 
        
        # In case of 'Sair' or else
        # return terminal state ((0, None, None), 1, total)
        else:
            prob = 1
            reward = total
            reachable.append(((0, None, None), prob, reward))
            return reachable
        
        # END_YOUR_CODE

    def discount(self):
        """
        Return the descount  that is 1
        """
        return 1
    
# **********************************************************
# **                    PART 02 Value Iteration           **
# **********************************************************

class ValueIteration(util.MDPAlgorithm):
    """ Asynchronous Value iteration algorithm """
    def __init__(self):
        self.pi = {}
        self.V = {}

    def solve(self, mdp, epsilon=0.001):
        """
        Solve the MDP using value iteration.  Your solve() method must set
        - self.V to the dictionary mapping states to optimal values
        - self.pi to the dictionary mapping states to an optimal action
        Note: epsilon is the error tolerance: you should stop value iteration when
        all of the values change by less than epsilon.
        The ValueIteration class is a subclass of util.MDPAlgorithm (see util.py).
        """
        mdp.computeStates()
        def computeQ(mdp, V, state, action):
            # Return Q(state, action) based on V(state).
            return sum(prob * (reward + mdp.discount() * V[newState]) \
                            for newState, prob, reward in mdp.succAndProbReward(state, action))

        def computeOptimalPolicy(mdp, V):
            # Return the optimal policy given the values V.
            pi = {}
            for state in mdp.states:
                pi[state] = max((computeQ(mdp, V, state, action), action) for action in mdp.actions(state))[1]
            return pi
        V = defaultdict(float)  # state -> value of state
        # Implement the main loop of Asynchronous Value Iteration Here:
        # BEGIN_YOUR_CODE
        
        def computeQValues(mdp, V):
            # Set MDP's sequence of values to zero
            for state in mdp.states:
                V[state] = 0
  
            delta = epsilon
    
            while True:
                biggest_change = 0
                for state in mdp.states:
                    V_new = max(computeQ(mdp, V, state, action) for action in mdp.actions(state))
                    change = abs(V[state] - V_new)
                    biggest_change = max(biggest_change, change)
                    V[state] = V_new
                
                # Break if epsilon (tolerance of changes in V) was reached
                delta = min(delta, biggest_change)
                if delta < epsilon:
                    break
                    
            return V
                
        V = computeQValues(mdp, V)

        # END_YOUR_CODE
        
        # Extract the optimal policy now
        pi = computeOptimalPolicy(mdp, V)
        # print("ValueIteration: %d iterations" % numIters)
        self.pi = pi
        self.V = V

# First MDP
MDP1 = BlackjackMDP(valores_cartas=[1, 5], multiplicidade=2, limiar=10, custo_espiada=1)

# Second MDP
MDP2 = BlackjackMDP(valores_cartas=[1, 5], multiplicidade=2, limiar=15, custo_espiada=1)

def geraMDPxereta():
    """
    Return an instance of BlackjackMDP where peeking is the
    optimal action for at least 10% of the states.
    """
    # BEGIN_YOUR_CODE
    return BlackjackMDP(valores_cartas=[3, 6, 15], multiplicidade=3, limiar=20, custo_espiada=1)
    # END_YOUR_CODE
    
# **********************************************************
# **                    PART 03 Q-Learning                **
# **********************************************************

class QLearningAlgorithm(util.RLAlgorithm):
    """
    Performs Q-learning.  Read util.RLAlgorithm for more information.
    actions: a function that takes a state and returns a list of actions.
    discount: a number between 0 and 1, which determines the discount factor
    featureExtractor: a function that takes a state and action and returns a
    list of (feature name, feature value) pairs.
    explorationProb: the epsilon value indicating how frequently the policy
    returns a random action
    """
    def __init__(self, actions, discount, featureExtractor, explorationProb=0.2):
        self.actions = actions
        self.discount = discount
        self.featureExtractor = featureExtractor
        self.explorationProb = explorationProb
        self.weights = defaultdict(float)
        self.numIters = 0

    def getQ(self, state, action):
        """
         Return the Q function associated with the weights and features
        """
        score = 0
        for f, v in self.featureExtractor(state, action):
            score += self.weights[f] * v
        return score

    def getAction(self, state):
        """
        Produce an action given a state, using the epsilon-greedy algorithm: with probability
        |explorationProb|, take a random action.
        """
        self.numIters += 1
        if random.random() < self.explorationProb:
            return random.choice(self.actions(state))
        return max((self.getQ(state, action), action) for action in self.actions(state))[1]

    def getStepSize(self):
        """
        Return the step size to update the weights.
        """
        return 1.0 / math.sqrt(self.numIters)

    def incorporateFeedback(self, state, action, reward, newState):
        """
         We will call this function with (s, a, r, s'), which you should use to update |weights|.
         You should update the weights using self.getStepSize(); use
         self.getQ() to compute the current estimate of the parameters.

         HINT: Remember to check if s is a terminal state and s' None.
        """
        # BEGIN_YOUR_CODE
        
        # Q value on current state
        Q = self.getQ(state, action)
        
        # Greedy search on Q, find action which maximizes Q
        if (newState != None):
            maxQ = max([self.getQ(newState, action) for action in self.actions(newState)])
        else:
            maxQ = Q
            
        # Discount factor
        gama =  self.discount
        # Learning rate
        alpha = self.getStepSize()
        
        # Update of Q according to Reinforcement Learning Algorithm
        Q_update = - alpha * Q + alpha * (reward + gama * maxQ)
        for feature_key, feature_value in self.featureExtractor(state, action):
            self.weights[feature_key] += Q_update * feature_value  

        # END_YOUR_CODE

def identityFeatureExtractor(state, action):
    """
    Return a single-element list containing a binary (indicator) feature
    for the existence of the (state, action) pair.  Provides no generalization.
    """
    featureKey = (state, action)
    featureValue = 1
    return [(featureKey, featureValue)]

# Large test case
largeMDP = BlackjackMDP(valores_cartas=[1, 3, 5, 8, 10], multiplicidade=3, limiar=40, custo_espiada=1)

# **********************************************************
# **        PART 03-01 Features for Q-Learning             **
# **********************************************************

def blackjackFeatureExtractor(state, action):
    """
    You should return a list of (feature key, feature value) pairs.
    (See identityFeatureExtractor() above for a simple example.)
    """
    # BEGIN_YOUR_CODE
    total, peek, deck_counter = state
    features = []
    
    # Action and current total 
    key = (action, total)
    value = 1
    features.append((key, value))
    
    if (deck_counter != None):   
        # Action and binary indicating presence/absence of each face value in the deck
        presence = [1 if counter > 0 else 0 for counter in deck_counter]
        key = (action, tuple(presence))
        features.append((key, value))
        
        for idx in range(len(deck_counter)):
            # Action and number of valores_cartas(idx) cards in the deck
            number = deck_counter[idx]
            key = (action, number)
            features.append((key, value))
        
    return features
    # END_YOUR_CODE
    
    
    
############ Simulations requested



#### Peeking percentage

#mdpP = geraMDPxereta()
# vi = ValueIteration()
# vi.solve(mdpP)
# esp = len([a for a in vi.pi.values() if a == 'Espiar'])
# tot = len(vi.pi.values()) 
# print("Peeking percentage: \t{}%\n".format(esp/tot*100))

# Peeking percentage: 11.76470588235294%



#### Value Iteration vs. Q-Learning Algorithm

#epochs = 30000

# # First MDP
# MDP1 = BlackjackMDP(valores_cartas=[1, 5], multiplicidade=2, limiar=10, custo_espiada=1)

# vi = ValueIteration()
# vi.solve(MDP1)
# pi_vi = vi.pi

# ql = QLearningAlgorithm(MDP1.actions, MDP1.discount(), identityFeatureExtractor)
# util.simulate(MDP1, ql, epochs)
# ql.explorationProb = 0
# same = 0

# for state in MDP1.states:
#     if pi_vi[state] == ql.getAction(state):
#         same += 1
# print("Percentage of equality: ", same/len(MDP1.states)*100)

# # Second MDP
# MDP2 = BlackjackMDP(valores_cartas=[1, 5], multiplicidade=2, limiar=15, custo_espiada=1)
# epochs = 30000

# vi = ValueIteration()
# vi.solve(MDP2)
# pi_vi = vi.pi

# ql = QLearningAlgorithm(MDP2.actions, MDP2.discount(), identityFeatureExtractor)
# util.simulate(MDP2, ql, epochs)
# ql.explorationProb = 0
# same = 0

# for state in MDP2.states:
#     if pi_vi[state] == ql.getAction(state):
#         same += 1
# print("Percentage of equality: \t{}%".format(same/len(MDP2.states)*100))

## Percentage of equality: 76.19047619047619%
## Percentage of equality:  59.09090909090909%



#### Q-Learning trained on MDP1 and test on largeMDP

# epochs = 30000
# vi = ValueIteration()

# First MDP
# MDP1 = BlackjackMDP(valores_cartas=[1, 5], multiplicidade=2, limiar=10, custo_espiada=1)

# vi.solve(MDP1)
# ql1 = QLearningAlgorithm(MDP1.actions, MDP1.discount(), identityFeatureExtractor)
# util.simulate(largeMDP, ql1, epochs)
# ql1.explorationProb = 0

# Large test case
# largeMDP = BlackjackMDP(valores_cartas=[1, 3, 5, 8, 10], multiplicidade=3, limiar=40, custo_espiada=1)

# vi.solve(largeMDP)
# ql_large = QLearningAlgorithm(largeMDP.actions, largeMDP.discount(), identityFeatureExtractor)
# util.simulate(largeMDP, ql_large, epochs)
# ql_large.explorationProb = 0

# equal = 0
# for state in largeMDP.states:
#     if ql1.getAction(state) == ql_large.getAction(state):
#         equal += 1
# print("Percentage of equality: \t{}%".format((equal/len(largeMDP.states))*100))

## Percentage of equality:  99.66728280961183%



#### IdentityFeatureExtractor vs. BlackjackFeatureExtractor

# Large test case
# largeMDP = BlackjackMDP(valores_cartas=[1, 3, 5, 8, 10], multiplicidade=3, limiar=40, custo_espiada=1)

# vi.solve(largeMDP)
# ql_opt = QLearningAlgorithm(largeMDP.actions, largeMDP.discount(), identityFeatureExtractor)
# util.simulate(largeMDP, ql_opt, epochs)
# ql_opt.explorationProb = 0


# vi.solve(largeMDP)
# ql_feat = QLearningAlgorithm(largeMDP.actions, largeMDP.discount(), blackjackFeatureExtractor)
# util.simulate(largeMDP, ql_feat, epochs)
# ql_feat.explorationProb = 0

# equal = 0
# for state in largeMDP.states:
#     if ql_feat.getAction(state) == ql_opt.getAction(state):
#         equal += 1
# print("Percentage of equality: \t{}%".format((equal/len(largeMDP.states))*100))

## Percentage of equality:  31.1275415896488%