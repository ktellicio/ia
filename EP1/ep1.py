"""
  AO PREENCHER ESSE CABECALHO COM O MEU NOME E O MEU NUMERO USP,
  DECLARO QUE SOU A UNICA PESSOA AUTORA E RESPONSAVEL POR ESSE PROGRAMA.
  TODAS AS PARTES ORIGINAIS DESSE EXERCICIO PROGRAMA (EP) FORAM
  DESENVOLVIDAS E IMPLEMENTADAS POR MIM SEGUINDO AS INSTRUCOES
  DESSE EP E, PORTANTO, NAO CONSTITUEM ATO DE DESONESTIDADE ACADEMICA,
  FALTA DE ETICA OU PLAGIO.
  DECLARO TAMBEM QUE SOU A PESSOA RESPONSAVEL POR TODAS AS COPIAS
  DESSE PROGRAMA E QUE NAO DISTRIBUI OU FACILITEI A
  SUA DISTRIBUICAO. ESTOU CIENTE QUE OS CASOS DE PLAGIO E
  DESONESTIDADE ACADEMICA SERAO TRATADOS SEGUNDO OS CRITERIOS
  DIVULGADOS NA PAGINA DA DISCIPLINA.
  ENTENDO QUE EPS SEM ASSINATURA NAO SERAO CORRIGIDOS E,
  AINDA ASSIM, PODERAO SER PUNIDOS POR DESONESTIDADE ACADEMICA.

  Nome : Karlson Tellicio Bezerra de Lima
  NUSP : 11585887

  Referencias: Com excecao das rotinas fornecidas no enunciado
  e em sala de aula, caso voce tenha utilizado alguma referencia,
  liste-as abaixo para que o seu programa nao seja considerado
  plagio ou irregular.

  Exemplo:
  - O algoritmo Quicksort foi baseado em:
  https://pt.wikipedia.org/wiki/Quicksort
  http://www.ime.usp.br/~pf/algoritmos/aulas/quick.html
"""

import util

def getRealCosts(corpus='corpus.txt'):

    """ Retorna as funcoes de custo unigrama, bigrama e possiveis fills obtidas a partir do corpus."""
    
    _realUnigramCost, _realBigramCost, _possibleFills = None, None, None
    if _realUnigramCost is None:
        print('Training language cost functions [corpus: '+ corpus+']... ')
        
        _realUnigramCost, _realBigramCost = util.makeLanguageModels(corpus)
        _possibleFills = util.makeInverseRemovalDictionary(corpus, 'aeiou')

        print('Done!')

    return _realUnigramCost, _realBigramCost, _possibleFills

############################################################
# Part 1: Segmentation problem under a unigram model

class SegmentationProblem(util.Problem):
    def __init__(self, query, unigramCost):
        self.query = query
        self.unigramCost = unigramCost

    def isState(self, state):
            return (0 <= state) & (state <= len(self.query)-1)

    def initialState(self):
        """ Metodo que implementa retorno da posicao inicial """
        # Initial state: index 0
        return 0

    def actions(self, state):
        """ Metodo que implementa retorno da 
        lista de acoes validas para um determinado estado """
        # Valid actions: every index from 1 to remaining query size
        actions = []
        for idx in range(1, len(self.query)+1 - state):
            actions.append(str(idx))
        return actions


    def nextState(self, state, action):
        """ Metodo que implementa funcao de transicao """ 
        newState = int(state) + int(action)
        return newState


    def isGoalState(self, state):
        """ Metodo que implementa teste de meta """
        # Final state: when final of query index has been reached
        return state == len(self.query)

    def stepCost(self, state, action):
        """ Metodo que implementa funcao custo """
        
        nextState = int(state) + int(action)
        
        # New word: on 'query', from index 'state' until index 'nextState'
        newWord = self.query[state:nextState]
        
        # Cost according to Unigram
        stepCost = self.unigramCost(newWord)

        return stepCost


def segmentWords(query, unigramCost):

    if len(query) == 0:
        return ''

    problem = SegmentationProblem(query, unigramCost)
    goal = util.uniformCostSearch(problem)

    val, sol = util.getSolution(goal, problem)
    actionsString = sol.strip(' ').split(' ')
    
    actions = [0]
    for action in actionsString:
        actions.append(int(action))
    
    words = []
    state = 0
    for i in range(len(actions) - 1):
        state += actions[i]
        words.append(query[state : state + actions[i+1]])
        
    return " ".join(words)


import util

import util

############################################################
# Part 2: Vowel insertion problem under a bigram cost

class VowelInsertionProblem(util.Problem):
    def __init__(self, queryWords, bigramCost, possibleFills):
        self.queryWords = queryWords
        self.bigramCost = bigramCost
        self.possibleFills = possibleFills

    def isState(self, state):
        """ Metodo  que implementa verificacao de estado """
        prevWord, idx = state
        return (0 <= idx) & (idx <= len(self.queryWords)-1)

    def initialState(self):
        """ Metodo que implementa retorno da posicao inicial """
        # # Initial state: first word, index 0
        return util.SENTENCE_BEGIN, 0
    
    def actions(self, state):
        """ Metodo  que implementa retorno da lista de acoes validas
        para um determinado estado
        """
        # Valid actions: every possible fill (or the very unfilled word, if no possible fill was found)
        prevWord, idx = state
        unfilled = self.queryWords[int(idx)]
        possibleFills = self.possibleFills(unfilled)
        
        if len(possibleFills) == 0:
            actions = [unfilled]
        else:
            actions = list(possibleFills)
        
        return actions

    def nextState(self, state, action):
        """ Metodo que implementa funcao de transicao """
        # Replace possible fill as the previous word and increment idx
        prevWord, idx = state
        newPrevWord = action

        if action in self.actions(state):
            newState = newPrevWord, str(int(idx) + 1)
        else:
            newState = None
        
        return newState
        
    
    def isGoalState(self, state):
        """ Metodo que implementa teste de meta """
        prevWord, idx = state
        return int(idx) == len(self.queryWords)
    
    def stepCost(self, state, action):
        """ Metodo que implementa funcao custo """
        # State: previous word,  index of current word
        prevWord, idx = state
        # Action: possible fill to replace word in 'index'
        pf = action
        
        stepCost = self.bigramCost(prevWord, pf)
        
        if action in self.actions(state):
            stepCost = self.bigramCost(prevWord, pf)
            
        else:
            stepCost = None
            
        return stepCost
        


def insertVowels(queryWords, bigramCost, possibleFills):
    # BEGIN_YOUR_CODE 
    # Voce pode usar a função getSolution para recuperar a sua solução a partir do no meta
    # valid,solution  = util.getSolution(goalNode,problem)
    #  raise NotImplementedError  

    if len(queryWords) == 0:
        return ''

    problem = VowelInsertionProblem(queryWords, bigramCost, possibleFills)
    goal = util.uniformCostSearch(problem)

    val, sol = util.getSolution(goal, problem)
    
    if val:
        return sol
    else:
        return None
    
    # END_YOUR_CODE

############################################################
def getRealCostsPortuguese(corpus='corpus_portuguese.txt'):

    """ Retorna as funcoes de custo unigrama, bigrama e possiveis fills 
    obtidas a partir do corpus de palavras em portugues."""
    
    _realUnigramCost, _realBigramCost, _possibleFills = None, None, None
    if _realUnigramCost is None:
        print('Training portuguese cost functions [corpus: '+ corpus+']... ')
        
        _realUnigramCost, _realBigramCost = util.makeLanguageModels(corpus)
        _possibleFills = util.makeInverseRemovalDictionary(corpus, 'aeiou')

        print('Done!')

    return _realUnigramCost, _realBigramCost, _possibleFills

def main():
    """ Voce pode/deve editar o main() para testar melhor sua implementacao.
    A titulo de exemplo, incluimos apenas algumas chamadas simples para
    lhe dar uma ideia de como instanciar e chamar suas funcoes.
    Descomente as linhas que julgar conveniente ou crie seus proprios testes.
    """
    unigramCost, bigramCost, possibleFills  =  getRealCosts()
    
    resulSegment = segmentWords('believeinyourselfhavefaithinyourabilities', unigramCost)
    print(resulSegment)

    resultInsert = insertVowels('smtms ltr bcms nvr'.split(), bigramCost, possibleFills)
    print(resultInsert)
  

# Use this main to test the portuguese version
#def main():
#    unigramCost, bigramCost, possibleFills  =  getRealCostsPortuguese()   
#    resulSegment = segmentWords('avidademachadodeassis', unigramCost)
#    print(resulSegment)
#    resultInsert = insertVowels('pqn vd d m grnd hmm'.split(), bigramCost, possibleFills)
#    print(resultInsert)

if __name__ == '__main__':
    main()


if __name__ == '__main__':
    main()
