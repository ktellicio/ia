%%%%% Insira aqui os seus predicados.
%%%%% Use quantos predicados auxiliares julgar necessário


% Ex.1
% Say: Xs, Cs: list
% and conj(x): unique values of X, in order of apperance
% Cs == conj(Xs)
% If debuging, uncomment the lines below.

lista_para_conjunto(Xs, Cs) :- lista_para_conjunto( Xs, [], Cs).

% If debuging, uncomment the line below and comment the line below that.
%lista_para_conjunto([],Cs, Cs) :- write('Done!'), nl.
lista_para_conjunto([],Cs, Cs).
lista_para_conjunto([Head|Tail], Aux, Cs) :- member(Head,Aux), 
%    							write(Head), write(' is already member of '), write(Aux), nl, 
    							lista_para_conjunto(Tail,Aux,Cs), !.
lista_para_conjunto([Head|Tail],Aux, Cs) :- not(member(Head,Aux)),
%    							write(Head), write(' is not member of '), write(Aux), nl,
    							append(Aux, [Head], Appended),
    							lista_para_conjunto(Tail,Appended,Cs).




% Ex.2
% Say: Cs, Ds: set
% Cs == Ds
% If debuging, uncomment the lines below.

mesmo_conjunto(Cs,Ds) :- contain(Cs,Ds), contain(Ds,Cs).

% If debuging, uncomment the line below and comment the line below that.
%contain([],Ds) :- write(Ds), write(' contains the other!'), nl.
contain([],_).

contain([Head|Tail], Ds) :-member(Head,Ds), 
%    					write(Ds), write(' contains '), write(Head), nl,
    					contain(Tail,Ds), !.
contain([Head|_], Ds) :- not(member(Head,Ds)), 
%    					write(Ds), write(' does not contain '), write(Head), nl, 
    					false.




% Ex.3
% Say: Es, Cs, Ds: set
% Es = Cs U Ds.
% If debuging, uncomment the lines below.

uniao_conjunto(Cs, Ds, Es) :- uniao_conjunto(Cs, Ds, [], Es).

uniao_conjunto(Cs, [], Es, Aux) :- append(Cs, Es, Aux).
%    							write('Done!'), nl.

uniao_conjunto(Cs, [Head|Tail], Es, Aux) :- member(Head,Cs), 
%    							write(Cs), write(' already contains '), write(Head), nl, 
    							uniao_conjunto(Cs,Tail,Es,Aux), !.
uniao_conjunto(Cs, [Head|Tail], Es, Aux) :- not(member(Head,Cs)),
%    							write(Cs), write(' does not contain '), write(Head), nl,
    							append(Es, [Head], Appended),
    							uniao_conjunto(Cs,Tail,Appended, Aux).




% Ex.4
% Say: Es, Cs, Ds: set
% Es = Cs ∩ Ds.
% If debuging, uncomment the lines below.

inter_conjunto(Cs, Ds, Es) :- inter_conjunto(Cs, Ds, [], Es).


inter_conjunto(_, [], Es, Aux) :- append([], Es, Aux).
%    							write(Aux), write(' is the intersection!'), nl.

inter_conjunto(Cs, [Head|Tail], Es, Aux) :- member(Head,Cs), 
%    							write(Head), write(' is in the intersection!'), nl, 
    							append(Es, [Head], Appended),
    							inter_conjunto(Cs,Tail,Appended, Aux), !.
inter_conjunto(Cs, [Head|Tail], Es, Aux) :- not(member(Head,Cs)),
%    							write(Head), write(' is not the intersection!'), nl, 
    							inter_conjunto(Cs,Tail,Es, Aux).




% Ex. 5
% Say: Es, Cs, Ds: set
% Es = Cs - Ds.
% If debuging, uncomment the lines below.

diferenca_conjunto(Cs,Ds,Es) :- diferenca_conjunto(Cs, Ds, [], Es).


diferenca_conjunto([], _, Es, Aux) :- append([], Es, Aux).
%    							write(Aux), write(' is the intersection!'), nl.

diferenca_conjunto([Head|Tail], Ds, Es, Aux) :- member(Head,Ds), 
%    							write(Head), write(' was subtracted from '), write([Head|Tail]), nl, 
    							diferenca_conjunto(Tail,Ds, Es, Aux), !.
diferenca_conjunto([Head|Tail], Ds, Es, Aux) :- not(member(Head,Ds)),
%    							write(Head), write(' stays!'), nl,
    							append(Es, [Head], Appended),
    							diferenca_conjunto(Tail,Ds,Appended,Aux).











%%%%%%%% Fim dos predicados adicionados
%%%%%%%% Os testes começam aqui.
%%%%%%%% Para executar os testes, use a consulta:   ?- run_tests.

%%%%%%%% Mais informacoes sobre testes podem ser encontradas em:
%%%%%%%%    https://www.swi-prolog.org/pldoc/package/plunit.html

:- begin_tests(conjuntos).
test(lista_para_conjunto, all(Xs=[[1,a,3,4]]) ) :-
    lista_para_conjunto([1,a,3,3,a,1,4], Xs).
test(lista_para_conjunto2,fail) :-
    lista_para_conjunto([1,a,3,3,a,1,4], [a,1,3,4]).

test(mesmo_conjunto, set(Xs=[[1,a,3],[1,3,a],[a,1,3],[a,3,1],[3,a,1],[3,1,a]])) :-
    mesmo_conjunto([1,a,3], Xs).
test(uniao_conjunto2,fail) :-
    mesmo_conjunto([1,a,3,4], [1,3,4]).

test(uniao_conjunto, set(Ys==[[1,a,3],[1,3,a],[a,1,3],[a,3,1],[3,a,1],[3,1,a]])) :-
    uniao_conjunto([1,a], [a,3], Xs),
    mesmo_conjunto(Xs,Ys).
test(uniao_conjunto2,fail) :-
    uniao_conjunto([1,a,3,4], [1,2,3,4], [1,1,a,2,3,3,4,4]).

test(inter_conjunto, all(Xs==[[1,3,4]])) :-
    inter_conjunto([1,a,3,4], [1,2,3,4], Xs).
test(inter_conjunto2,fail) :-
    inter_conjunto([1,a,3,4], [1,2,3,4], [1,1,3,3,4,4]).

test(diferenca_conjunto, all(Xs==[[2]])) :-
    diferenca_conjunto([1,2,3], [3,a,1], Xs).
test(diferenca_conjunto2,fail) :-
    diferenca_conjunto([1,3,4], [1,2,3,4], [_|_]).

:- end_tests(conjuntos).
